# Minecraft PaperMC Sever - Quick Install (At the moment, its only for linux)

Script used to install a Minecraft Paper server in seconds. (With Paper Auto-Update)

## Installation

You will need these packages to use the script:

```sh
sudo apt install opendjk-17-jre-headless curl jq -y
```

## How to use?

Run [install.sh](install.sh) to get the latest version of PaperMC

```sh
./install.sh
```

## Parameters

You can edit the [script](install.sh) to choose a specific version:

Remove

```sh
version="$(curl -sX GET https://papermc.io/api/v2/projects/paper | jq -r '.version [-1]')"
```

, and add (Exemple)

```sh
version="1.18.1"
```

## Credits

PaperMC API : [http://papermc.io/api](https://papermc.io/api/docs/swagger-ui/index.html?configUrl=/api/openapi/swagger-config#/)

## License

[MIT](LICENSE)

MIT © [Michael François](https://gitlab.com/michael.francois)
