#!/bin/bash

# Minecraft Server parameters
version="$(curl -sX GET https://papermc.io/api/v2/projects/paper | jq -r '.versions [-1]')" # Delete the request in braquets and write the wanted version if you want to choose a specific one (Ex: "1.12.2")
jar_file="paper.jar"

# JVM parameters
java_maximum_memory=4G
java_maximum_initial_memory=4G


# Use for CI, ignore it
[[ $1 = ci ]] && jar_file="paper-ci_test.jar"

# Replace the last executable if it is not up to date 
latest_build="$(curl -sX GET https://papermc.io/api/v2/projects/paper/versions/$version | jq '.builds [-1]')"

[ -f $jar_file ] && current_checksum="$(sha256sum $jar_file | awk '{print $1}')"
latest_checksum="$(curl -s https://papermc.io/api/v2/projects/paper/versions/$version/builds/$latest_build | jq -r '.downloads.application.sha256')"

if [[ $current_checksum != $latest_checksum ]]; then
    [ -f $jar_file ] && rm $jar_file
    wget -q "https://papermc.io/api/v2/projects/paper/versions/$version/builds/$latest_build/downloads/paper-$version-$latest_build.jar" -O $jar_file
fi


# Launch the server
echo "eula=true" >> eula.txt
if [[ $1 != ci ]]; then
    java -Xms$java_maximum_initial_memory -Xmx$java_maximum_memory -jar $jar_file
fi