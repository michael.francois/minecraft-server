# JVM Parameters
jar_file="paper.jar"
max_memory=4G
max_memory_startup=4G


java -Xms$max_memory_startup -Xmx$max_memory -jar $jar_file
